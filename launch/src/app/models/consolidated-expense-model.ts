
export class ConsolidatedExpenseModel {
    valor: number;
    mes_lancamento: number;

    constructor(valor: number, mes: number){
        this.valor = valor;
        this.mes_lancamento = mes;
    }
}
