import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTabsModule, MatTableModule} from '@angular/material'

let modules = [ MatTableModule, MatTabsModule]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    modules
  ],
  exports: modules

})
export class AngMaterialModule { }