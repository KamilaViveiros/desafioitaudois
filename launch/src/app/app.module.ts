import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ConsolidatedExpenseListComponent } from './pages/consolidated-expense-list/consolidated-expense-list.component';
import { GeneralExpenseListComponent } from './pages/general-expense-list/general-expense-list.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngMaterialModule } from './ang-material.module';



@NgModule({
  declarations: [
    AppComponent,
    ConsolidatedExpenseListComponent,
    GeneralExpenseListComponent,
   
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
