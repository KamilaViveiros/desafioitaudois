import { Injectable } from '@angular/core';
import { ExpenseRepository } from '../repository/expense-repository';
import { element } from 'protractor';
import { GeneralExpenseModel } from '../models/general-expense-model';
import { ConsolidatedExpenseModel } from '../models/consolidated-expense-model';

@Injectable({
  providedIn: 'root'
})
export class ConsolidatedExpenseService {

  constructor(private _expenseRespository: ExpenseRepository) { }

  get() {
    return this._expenseRespository.get();
  }

  consolidateExpense(expenses: GeneralExpenseModel[]) {
    let consolited = new Array<ConsolidatedExpenseModel>();

    expenses.forEach((expense) => {
      this.monthExists(
        expense.mes_lancamento,
        consolited,
        (element) => this.calculatedConsolidatedExpense(expense.valor, element),
        () => this.addConsolidatedExpenses(expense, consolited));
    })
    return consolited.sort((a, b) => a.mes_lancamento > b.mes_lancamento ? 1 : -1)
  }


  private monthExists(indexMonth: number, consolidatedExpenses: ConsolidatedExpenseModel[], callbackTrue: (element: ConsolidatedExpenseModel) => void, callbackFalse: () => void) {
    for (let index = 0; index < consolidatedExpenses.length; index++) {
      const element = consolidatedExpenses[index];
      if (element.mes_lancamento == indexMonth)
        return callbackTrue(element);
    }

    callbackFalse();
  }

  private addConsolidatedExpenses(generalExpense: GeneralExpenseModel, consolidatedExpenses: ConsolidatedExpenseModel[]) {
    consolidatedExpenses.push(new ConsolidatedExpenseModel(generalExpense.valor, generalExpense.mes_lancamento));
  }

  private calculatedConsolidatedExpense(valor: number, consolidatedExpense: ConsolidatedExpenseModel) {
    consolidatedExpense.valor += valor;
  }

}
