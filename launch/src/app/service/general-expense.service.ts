import { Injectable } from '@angular/core';
import { ExpenseRepository } from '../repository/expense-repository';

@Injectable({
  providedIn: 'root'
})
export class GeneralExpenseService {

  constructor(private _expenseRepository: ExpenseRepository) { }

  get(){
    return this._expenseRepository.get();
  }
}
