import { Component, OnInit } from '@angular/core';
import { ConsolidatedExpenseService } from 'src/app/service/consolidated-expense.service';
import { ConsolidatedExpenseModel } from 'src/app/models/consolidated-expense-model';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-consolidated-expense-list',
  templateUrl: './consolidated-expense-list.component.html',
  styleUrls: ['./consolidated-expense-list.component.css']
})
export class ConsolidatedExpenseListComponent implements OnInit {

  constructor(private expenseService : ConsolidatedExpenseService) { }
  displayedColumns: string[] = ['valor', 'mes_lancamento'];
  dataSource: MatTableDataSource<ConsolidatedExpenseModel>;
  months = ['Janeiro', 'Fevereiro', 'Março', 'Abrir', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
  
  list: ConsolidatedExpenseModel  []
  ngOnInit() {
    this.get()

  }

  get(){
    this.expenseService.get().subscribe((response) => {
      
      this.dataSource = new MatTableDataSource<ConsolidatedExpenseModel>(this.expenseService.consolidateExpense(response))
    })
  }

}
