import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidatedExpenseListComponent } from './consolidated-expense-list.component';

describe('ConsolidatedExpenseListComponent', () => {
  let component: ConsolidatedExpenseListComponent;
  let fixture: ComponentFixture<ConsolidatedExpenseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolidatedExpenseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolidatedExpenseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
