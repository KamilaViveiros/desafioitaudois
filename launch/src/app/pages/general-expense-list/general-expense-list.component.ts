import { Component, OnInit } from '@angular/core';
import { GeneralExpenseService } from 'src/app/service/general-expense.service';
import { GeneralExpenseModel } from 'src/app/models/general-expense-model';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-general-expense-list',
  templateUrl: './general-expense-list.component.html',
  styleUrls: ['./general-expense-list.component.css']
})
export class GeneralExpenseListComponent implements OnInit {

  constructor(private expenseService : GeneralExpenseService) { }
  displayedColumns: string[] = ['origem', 'categoria', 'valor', 'mes_lancamento'];
  dataSource: MatTableDataSource<GeneralExpenseModel>;
  category = ['Um', 'Dois','Três', 'Quatro', 'Cinco', 'Seis','Sete', 'Oito', 'Nove', 'Dez'];
  months = ['Janeiro', 'Fevereiro', 'Março', 'Abrir', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
  ngOnInit() {
    this.getGeneralList()

  }

  getGeneralList(){
    this.expenseService.get().subscribe((generalList: GeneralExpenseModel[]) => {
      this.dataSource = new MatTableDataSource<GeneralExpenseModel>(generalList.sort((a, b) => a.mes_lancamento > b.mes_lancamento ? 1 : -1))

    })
  }

}
