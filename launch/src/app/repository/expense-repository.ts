import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralExpenseModel } from '../models/general-expense-model';

@Injectable({
  providedIn: 'root'
})
export class ExpenseRepository {

  constructor(private _httpClient: HttpClient) { }
  
  url = 'https://desafio-it-server.herokuapp.com/lancamentos'; 

  get(){
    return this._httpClient.get<GeneralExpenseModel[]>(this.url)
  }
}
